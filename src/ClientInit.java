import javax.swing.UIManager;

import com.aposbot.BotFrame;
import com.aposbot.BotLoader;
import com.aposbot._default.IAutoLogin;
import com.aposbot._default.IClient;
import com.aposbot._default.IClientInit;
import com.aposbot._default.IPaintListener;
import com.aposbot._default.IScriptListener;
import com.aposbot._default.ISleepListener;
import com.aposbot.shit.AutoLogin;

public final class ClientInit extends com.aposbot.shit.ClientInit {

    public static void main(String[] argv) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (final Throwable t) {
        } finally {
            loader = new BotLoader(argv, new ClientInit());
        }
    }

    @Override
    public IClient createClient(BotFrame frame) {
        //client.il[237] = "Welcome to @cya@APOS";
        final Extension ex = new Extension(frame);
        ScriptListener.init(ex);
        AutoLogin.init(ex);
        PaintListener.init(ex);
        return ex;
    }
}
