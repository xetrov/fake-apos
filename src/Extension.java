import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Arrays;

import javax.script.Invocable;

import client.GameShell;
import client.Version;
import client.mudclient;
import com.aposbot.BotFrame;
import com.aposbot._default.IAutoLogin;
import com.aposbot._default.IClient;
import com.aposbot._default.IJokerFOCR;
import com.aposbot._default.IPaintListener;
import com.aposbot._default.IScript;
import com.aposbot._default.IScriptListener;
import com.aposbot._default.IStaticAccess;

public class Extension extends mudclient
	implements IClient {

	private static final long serialVersionUID = 1L;
	private final BotFrame frame;
	private boolean disableKeys;

	Extension(BotFrame frame) {
		this.frame = frame;
	}

	@Override
	public void onLoggedIn() {
		frame.enableRenderingToggle();
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(gameWidth, gameHeight + 12);
	}

	private void paste() {
		try {
			Clipboard c = Toolkit.getDefaultToolkit()
					.getSystemClipboard();
			Transferable t = c.getContents(null);
			DataFlavor f = DataFlavor.stringFlavor;
			if (!t.isDataFlavorSupported(f)) {
				return;
			}
			char[] str = ((String)t.getTransferData(f))
					.toCharArray();
			for (int i = 0; i < str.length; ++i) {
				if (str[i] >= ' ' && str[i] <= '~') {
					typeChar(str[i], str[i]);
				}
			}
		} catch (Throwable t) {
			System.out.println("Couldn't paste: " + t);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		final int keyCode = e.getKeyCode();
		if (disableKeys) {
			ScriptListener.get().onKeyPress(keyCode);
			return;
		}
		if ((e.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0) {
			if (e.getKeyCode() == KeyEvent.VK_V) {
				paste();
				return;
			}
		}
		switch (keyCode) {
			case KeyEvent.VK_ESCAPE:
				setCameraNSOffset(0);
				setCameraEWOffset(0);
				setCameraHeight(750);
				//fogginess = 0;
				break;
			case KeyEvent.VK_PAGE_UP:
				setCameraNSOffset(getCameraNSOffset() - 10);
				break;
			case KeyEvent.VK_PAGE_DOWN:
				setCameraNSOffset(getCameraNSOffset() + 10);
				break;
			case KeyEvent.VK_HOME:
				setCameraEWOffset(getCameraEWOffset() + 10);
				break;
			case KeyEvent.VK_END:
				setCameraEWOffset(getCameraEWOffset() - 10);
				break;
			case KeyEvent.VK_UP:
				final int h = getCameraHeight();
				if (h > 300) {
					setCameraHeight(h - 10);
					//fogginess -= 30;
				}
				break;
			case KeyEvent.VK_DOWN:
				setCameraHeight(getCameraHeight() + 10);
				//fogginess += 30;
				break;
			case KeyEvent.VK_F12:
			case KeyEvent.VK_PRINTSCREEN:
				new Thread(new Runnable() {
					@Override
					public void run() {
						takeScreenshot(String.valueOf(System.currentTimeMillis()));
					}
				}, "ScreenshotThread").start();
				break;
			default:
				ScriptListener.get().onKeyPress(keyCode);
				super.keyPressed(e);
				break;
		}
	}

	@Override
	public void setRendering(boolean b) {
		super.setRendering(b);
	}

	@Override
	public boolean isRendering() {
		return super.isRendering();
	}

	@Override
	public void setAutoLogin(boolean b) {
		super.setAutoLogin(b);
	}

	@Override
	public void setKeysDisabled(boolean b) {
		super.setKeysDisabled(b);
	}

	@Override
	public void stopScript() {
		frame.stopScript();
	}

	@Override
	public void takeScreenshot(String fileName) {
		super.takeScreenshot(fileName);
	}

	@Override
	public void setServer(int id) {
		super.setServer(id);
	}

	@Override
	public int getServer() {
		return super.getServer();
	}

	@Override
	public int getCameraNSOffset() {
		return super.cameraRotationY;
	}

	@Override
	public void setCameraNSOffset(int i) {
		super.cameraRotationY = i;
	}

	@Override
	public int getCameraEWOffset() {
		return super.cameraRotationX;
	}

	@Override
	public void setCameraEWOffset(int i) {
		super.cameraRotationX = i;
	}

	@Override
	public int getCameraHeight() {
		return super.cameraElev == 0 ? -super.world.getElevation(localPlayer.currentX + cameraRotationX, localPlayer.currentY + cameraRotationY) : super.cameraElev; // ??
	}

	@Override
	public void setCameraHeight(int i) {
		super.cameraElev = i; // ??
	}

	@Override
	public void setActionInd(int i) {
		// ??
	}

	@Override
	public void closeWelcomeBox() {
		super.showDialogWelcome = false;
	}

	@Override
	public int getLocalX() {
		return super.getLocalX();
	}

	@Override
	public int getLocalY() {
		return super.getLocalY();
	}

	@Override
	public void setLogoutTimer(int i) {
		super.logoutTimeout = i;
	}

	@Override
	public int getAreaX() {
		return super.getAreaX();
	}

	@Override
	public int getAreaY() {
		return super.getAreaY();
	}

	@Override
	public int getBaseLevel(int skill) {
		return super.getBaseLevel(skill);
	}

	@Override
	public int getCurrentLevel(int skill) {
		return super.getCurrentLevel(skill);
	}

	@Override
	public double getExperience(int skill) {
		return super.getExperience(skill) / 4.0; // why
	}

	@Override
	public int getCombatStyle() {
		return super.getCombatStyle();
	}

	@Override
	public void setCombatStyle(int i) {
		super.setCombatStyle(i);
	}

	@Override
	public String[] getDialogOptions() {
		return super.getDialogOptions();
	}

	@Override
	public int getDialogOptionCount() {
		return super.getDialogOptionCount();
	}

	@Override
	public boolean isDialogVisible() {
		return super.isDialogVisible();
	}

	@Override
	public void setDialogVisible(boolean b) {
		super.showOptionMenu = b;
	}

	@Override
	public boolean isSleeping() {
		return super.isSleeping();
	}

	@Override
	public int getInventorySize() {
		return super.getInventorySize();
	}

	@Override
	public int getInventoryId(int i) {
		return super.getInventoryId(i);
	}

	@Override
	public int getInventoryStack(int i) {
		return super.getInventoryStack(i);
	}

	@Override
	public boolean isBankVisible() {
		return super.isBankVisible();
	}

	@Override
	public void setBankVisible(boolean b) {
		super.showDialogBank = b;
	}

	@Override
	public int getBankSize() {
		return super.getBankSize();
	}

	@Override
	public int getBankId(int i) {
		return super.getBankId(i);
	}

	@Override
	public int getBankStack(int i) {
		return super.getBankStack(i);
	}

	@Override
	public int getGroundItemCount() {
		return super.getGroundItemCount();
	}

	@Override
	public int getGroundItemLocalX(int i) {
		return super.getGroundItemLocalX(i);
	}

	@Override
	public int getGroundItemLocalY(int i) {
		return super.getGroundItemLocalY(i);
	}

	@Override
	public int getGroundItemId(int i) {
		return super.getGroundItemId(i);
	}

	@Override
	public int getObjectCount() {
		return super.getObjectCount();
	}

	@Override
	public int getObjectLocalX(int i) {
		return super.getObjectLocalX(i);
	}

	@Override
	public int getObjectId(int i) {
		return super.getObjectId(i);
	}

	@Override
	public int getObjectDir(int i) {
		return super.getObjectDir(i);
	}

	@Override
	public int getBoundCount() {
		return super.getBoundCount();
	}

	@Override
	public int getBoundLocalX(int i) {
		return super.getBoundLocalX(i);
	}

	@Override
	public int getBoundLocalY(int i) {
		return super.getBoundLocalY(i);
	}

	@Override
	public int getBoundDir(int i) {
		return super.getBoundDir(i);
	}

	@Override
	public int getBoundId(int i) {
		return super.getBoundId(i);
	}

	@Override
	public void typeChar(char key_char, int key_code) {
		super.typeChar(key_char, key_code);
	}

	@Override
	public boolean isShopVisible() {
		return super.isShopVisible();
	}

	@Override
	public void setShopVisible(boolean b) {
		super.showDialogShop = b;
	}

	@Override
	public int getShopSize() {
		return super.getShopSize();
	}

	@Override
	public int getShopId(int i) {
		return super.getShopId(i);
	}

	@Override
	public int getShopStack(int i) {
		return super.getShopStack(i);
	}

	@Override
	public boolean isEquipped(int slot) {
		return super.isEquipped(slot);
	}

	@Override
	public boolean isPrayerEnabled(int id) {
		return super.isPrayerEnabled(id);
	}

	@Override
	public void setPrayerEnabled(int i, boolean flag) {
		super.setPrayerEnabled(i, flag);
	}

	@Override
	public boolean isInTradeOffer() {
		return super.isInTradeOffer();
	}

	@Override
	public void setInTradeOffer(boolean b) {
		super.setInTradeOffer(b);
	}

	@Override
	public boolean isInTradeConfirm() {
		return super.isInTradeConfirm();
	}

	@Override
	public void setInTradeConfirm(boolean b) {
		super.setInTradeConfirm(b);
	}

	@Override
	public boolean hasLocalAcceptedTrade() {
		return super.hasLocalAcceptedTrade();
	}

	@Override
	public boolean hasLocalConfirmedTrade() {
		return super.hasLocalConfirmedTrade();
	}

	@Override
	public boolean hasRemoteAcceptedTrade() {
		return super.hasRemoteAcceptedTrade();
	}

	@Override
	public int getLocalTradeItemCount() {
		return super.getLocalTradeItemCount();
	}

	@Override
	public int getLocalTradeItemId(int i) {
		return super.getLocalTradeItemId(i);
	}

	@Override
	public int getLocalTradeItemStack(int i) {
		return super.getLocalTradeItemStack(i);
	}

	@Override
	public int getRemoteTradeItemCount() {
		return super.getRemoteTradeItemCount();
	}

	@Override
	public int getRemoteTradeItemId(int i) {
		return super.getRemoteTradeItemId(i);
	}

	@Override
	public int getRemoteTradeItemStack(int i) {
		return super.getRemoteTradeItemStack(i);
	}

	@Override
	public int[] getPixels() {
		return super.getPixels();
	}

	@Override
	public Image getImage() {
		return super.surface.image;
	}

	@Override
	public boolean isLoggedIn() {
		return super.isLoggedIn();
	}

	@Override
	public int[][] getAdjacency() {
		return Arrays.copyOf(super.world.objectAdjacency, super.world.objectAdjacency.length);
	}

	@Override
	public void drawString(String str, int x, int y, int font, int colour) {
		super.drawString(str, x, y, font, colour);
	}

	@Override
	public void displayMessage(String str) {
		super.displayMessage(str);
	}

	@Override
	public void offerItemTrade(int slot, int amount) {
		super.offerItemTrade(slot, amount);
	}

	@Override
	public void login(String username, String password) {
		loginScreen = 2;
		super.login(username, password, false);
	}

	@Override
	public void walkDirectly(int destLX, int destLY, boolean action) {
		super.walkDirectly(destLX, destLY, action);
	}

	@Override
	public void walkAround(int destLX, int destLY) {
		super.walkAround(destLX, destLY);
	}

	@Override
	public void walkToBound(int destLX, int destLY, int dir) {
		super.walkToBound(destLX, destLY, dir);
	}

	@Override
	public void walkToObject(int destLX, int destLY, int dir, int id) {
		super.walkToObject(destLX, destLY, dir, id);
	}

	@Override
	public void createPacket(int opcode) {
		super.clientStream.newPacket(opcode);
	}

	@Override
	public void put1(int val) {
		super.clientStream.putByte(val);
	}

	@Override
	public void put2(int val) {
		super.clientStream.putShort(val);
	}

	@Override
	public void put4(int val) {
		super.clientStream.putInt(val);
	}

	@Override
	public void finishPacket() {
		super.clientStream.sendPacket();
	}

	@Override
	public void sendCAPTCHA(String str) {
		super.sendCAPTCHA(str);
	}

	@Override
	public boolean isSkipLines() {
		return super.isSkipLines();
	}

	@Override
	public void setSkipLines(boolean flag) {
		super.setSkipLines(flag);
	}

	@Override
	public void sendPrivateMessage(String msg, String name) {
		super.sendPrivateMessage(msg, name);
	}

	@Override
	public void addFriend(String str) {
		super.friendAdd(str);
	}

	@Override
	public void addIgnore(String str) {
		super.ignoreAdd(str);
	}

	@Override
	public void removeIgnore(String str) {
		super.ignoreRemove(str);
	}

	@Override
	public void removeFriend(String str) {
		super.friendRemove(str);
	}

	@Override
	public int getQuestCount() {
		return super.getQuestCount();
	}

	@Override
	public String getQuestName(int i) {
		return super.getQuestName(i);
	}

	@Override
	public boolean isQuestComplete(int i) {
		return super.isQuestComplete(i);
	}

	@Override
	public double getFatigue() {
		return super.getFatigue();
	}

	@Override
	public double getSleepingFatigue() {
		return super.getSleepingFatigue();
	}

	@Override
	public int getPlayerCount() {
		return super.getPlayerCount();
	}

	@Override
	public Object getPlayer() {
		return super.getPlayer();
	}

	@Override
	public Object getPlayer(int index) {
		return super.getPlayer(index);
	}

	@Override
	public String getPlayerName(Object mob) {
		return super.getPlayerName(mob);
	}

	@Override
	public int getPlayerCombatLevel(Object mob) {
		return super.getPlayerCombatLevel(mob);
	}

	@Override
	public int getNpcCount() {
		return super.getNpcCount();
	}

	@Override
	public Object getNpc(int index) {
		return super.getNpc(index);
	}

	@Override
	public int getNpcId(Object mob) {
		return super.getNpcId(mob);
	}

	@Override
	public boolean isMobWalking(Object mob) {
		return super.isMobWalking(mob);
	}

	@Override
	public boolean isMobTalking(Object mob) {
		return super.isMobTalking(mob);
	}

	@Override
	public boolean isHeadIconVisible(Object mob) {
		return super.isHeadIconVisible(mob);
	}

	@Override
	public boolean isHpBarVisible(Object mob) {
		return super.isHpBarVisible(mob);
	}

	@Override
	public int getMobBaseHitpoints(Object mob) {
		return super.getMobBaseHitpoints(mob);
	}

	@Override
	public int getMobServerIndex(Object mob) {
		return super.getMobServerIndex(mob);
	}

	@Override
	public int getMobLocalX(Object mob) {
		return super.getMobLocalX(mob);
	}

	@Override
	public int getMobLocalY(Object mob) {
		return super.getMobLocalY(mob);
	}

	@Override
	public int getMobDirection(Object mob) {
		return super.getMobDirection(mob);
	}

	@Override
	public boolean isMobInCombat(Object mob) {
		return super.isMobInCombat(mob);
	}

	@Override
	public IStaticAccess getStatic() {
		return StaticAccess.get();
	}

	@Override
	public IScriptListener getScriptListener() {
		return ScriptListener.get();
	}

	@Override
	public IAutoLogin getAutoLogin() {
		return AutoLogin.get();
	}

	@Override
	public IPaintListener getPaintListener() {
		return PaintListener.get();
	}

	@Override
	public IJokerFOCR getJoker() {
		return Joker.get();
	}

	@Override
	public IScript createInvocableScript(Invocable inv, String name) {
		return new JavaxScriptInvocable(this, inv, name);
	}

	@Override
	public void setFont(String name) {
		StaticAccess.loadFont(this, "h11p", name, 0);
		StaticAccess.loadFont(this, "h12b", name, 1);
		StaticAccess.loadFont(this, "h12p", name, 2);
		StaticAccess.loadFont(this, "h13b", name, 3);
		StaticAccess.loadFont(this, "h14b", name, 4);
		StaticAccess.loadFont(this, "h16b", name, 5);
		StaticAccess.loadFont(this, "h20b", name, 6);
		StaticAccess.loadFont(this, "h24b", name, 7);
	}

	@Override
	public void resizeGame(int width, int height) {
		super.resizeGame(width, height);
	}

	@Override
	public int getGameWidth() {
		return super.getGameWidth();
	}

	@Override
	public int getGameHeight() {
		return super.getGameHeight();
	}

	@Override
	public void destroy() {
		super.destroy();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
	}

	@Override
	public void init() {
		super.init();
	}

	@Override
	public void start() {
		super.start();
	}

	@Override
	public void startApplication(BotFrame b, int width, int height, String title, boolean resizeable) {
		super.appletMode = false;
		super.members = true;
		super.limit30 = false;
		super.server = "classic2.runescape.com";
		super.port = 43594;
		Version.CLIENT = 235;
		super.gameWidth = 1024;
		super.gameHeight = 668;
		Version.MAPS = Version.MAPS_233;
		Version.MEDIA = Version.MEDIA_233;
		super.threadSleep = 10;
		super.startApplication(width, height, title, resizeable);
		b.addMouseListener(this);
		b.addMouseMotionListener(this);
		b.addKeyListener(this);
	}
}
