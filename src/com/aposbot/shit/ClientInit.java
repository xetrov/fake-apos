package com.aposbot.shit;

import javax.swing.UIManager;

import com.aposbot.BotFrame;
import com.aposbot.BotLoader;
import com.aposbot._default.IAutoLogin;
import com.aposbot._default.IClient;
import com.aposbot._default.IClientInit;
import com.aposbot._default.IPaintListener;
import com.aposbot._default.IScriptListener;
import com.aposbot._default.ISleepListener;

public class ClientInit
        implements IClientInit {

    public static BotLoader loader;

    public ClientInit() {
    }

    public static BotLoader getBotLoader() {
        return loader;
    }

    @Override
    public IClient createClient(BotFrame frame) {
        return null;
    }

    @Override
    public IAutoLogin getAutoLogin() {
        return AutoLogin.get();
    }

    @Override
    public ISleepListener getSleepListener() {
        return SleepListener.get();
    }

    @Override
    public IScriptListener getScriptListener() {
        return ScriptListener.get();
    }

    @Override
    public IPaintListener getPaintListener() {
        return PaintListener.get();
    }
}
