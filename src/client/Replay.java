package client; /**
 *	rscplus
 *
 *	This file is part of rscplus.
 *
 *	rscplus is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	rscplus is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with rscplus.  If not, see <http://www.gnu.org/licenses/>.
 *
 *	Authors: see <https://github.com/OrN/rscplus>
 */

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.GZIPOutputStream;

public class Replay {
    // If we ever change replays in a way that breaks backwards compatibility, we need to increment this
    public static int VERSION = 0;

    static DataOutputStream output = null;
    static DataOutputStream input = null;
    static DataOutputStream keys = null;
    static DataOutputStream keyboard = null;
    static DataOutputStream mouse = null;

    static DataInputStream play_keys = null;
    static DataInputStream play_keyboard = null;
    static DataInputStream play_mouse = null;

    public static final byte KEYBOARD_TYPED = 0;
    public static final byte KEYBOARD_PRESSED = 1;
    public static final byte KEYBOARD_RELEASED = 2;

    public static final byte MOUSE_CLICKED = 0;
    public static final byte MOUSE_ENTERED = 1;
    public static final byte MOUSE_EXITED = 2;
    public static final byte MOUSE_PRESSED = 3;
    public static final byte MOUSE_RELEASED = 4;
    public static final byte MOUSE_DRAGGED = 5;
    public static final byte MOUSE_MOVED = 6;
    public static final byte MOUSE_WHEEL_MOVED = 7;

    public static final int DEFAULT_PORT = 43594;

    public static final int TIMESTAMP_EOF = -1;

    public static boolean isPlaying = false;
    public static boolean isRecording = false;
    public static boolean paused = false;
    public static boolean closeDialogue = false;

    // Hack for player position
    public static boolean ignoreFirstMovement = true;

    public static int fps = 50;
    public static float fpsPlayMultiplier = 1.0f;
    public static float prevFPSPlayMultiplier = fpsPlayMultiplier;
    public static int frame_time_slice;
    public static int connection_port;

    public static Thread replayThread = null;

    public static int replay_version;
    public static int client_version;
    public static int prevPlayerX;
    public static int prevPlayerY;

    public static int timestamp;
    public static int timestamp_client;
    public static int timestamp_server_last;
    public static int timestamp_kb_input;
    public static int timestamp_mouse_input;

    public static boolean started_record_kb_mouse = true;

    public static int enc_opcode;
    public static int retained_timestamp;
    public static byte[] retained_bytes = null;
    public static int retained_off;
    public static int retained_bread;

    public static int timestamp_lag = 0;

    public static void incrementTimestamp() {
        timestamp++;

        if (timestamp == TIMESTAMP_EOF) {
            timestamp = 0;
        }
    }

    public static void incrementTimestampClient() {
        timestamp_client++;

        if (timestamp_client == TIMESTAMP_EOF) {
            timestamp_client = 0;
        }
    }

    public static void init() {
        timestamp = 0;
        timestamp_client = 0;
        timestamp_server_last = 0;
    }

    public static int getServerLag() {
        return timestamp - timestamp_server_last;
    }

    public static void initializeReplayRecording(String username_login) {
        // No username specified, exit
        if (username_login.length() == 0)
            return;

        String timeStamp = new SimpleDateFormat("MM-dd-yyyy HH.mm.ss").format(new Date());

        try {
            String recordingDirectory = "replay/" + username_login;
            Files.createDirectories(Paths.get(recordingDirectory));
            recordingDirectory = recordingDirectory + "/" + timeStamp;
            Files.createDirectories(Paths.get(recordingDirectory));

            // Write out version information
            DataOutputStream version = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File(recordingDirectory + "/version.bin"))));
            version.writeInt(Replay.VERSION);
            version.writeInt(235);
            version.close();

            output = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(new File(recordingDirectory + "/out.bin.gz")))));
            input = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(new File(recordingDirectory + "/in.bin.gz")))));
            keys = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File(recordingDirectory + "/keys.bin"))));
            /*if (Settings.RECORD_KB_MOUSE) {
                keyboard = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(new File(recordingDirectory + "/keyboard.bin.gz")))));
                mouse = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(new File(recordingDirectory + "/mouse.bin.gz")))));
                started_record_kb_mouse = true; //need this to know whether or not to close the file if the user changes settings mid-recording
            } else {*/
                started_record_kb_mouse = false;
            //}

            System.out.println("client.client.Replay recording started");
        } catch (Exception e) {
            output = null;
            input = null;
            keys = null;
            keyboard = null;
            mouse = null;
            System.err.println("Unable to create replay files");
            return;
        }

        retained_timestamp = TIMESTAMP_EOF;
        retained_bytes = null;
        isRecording = true;
    }

    public static void closeReplayRecording() {
        if (input == null || !isRecording)
            return;

        isRecording = false;

        try {
            // since we are working with packet retention, last packet on memory has not been written,
            // write it here
            if (retained_timestamp != TIMESTAMP_EOF && retained_bytes != null) {
                try {
                    input.writeInt(retained_timestamp);
                    input.writeInt(retained_bread);
                    input.write(retained_bytes, retained_off, retained_bread);
                } catch (Exception e) {
                    e.printStackTrace();
                    shutdown_error();
                }
            }

            // Write EOF values
            input.writeInt(TIMESTAMP_EOF);
            output.writeInt(TIMESTAMP_EOF);

            System.out.println("Close streams");

            output.close();
            input.close();
            keys.close();
            if (started_record_kb_mouse) {
                keyboard.writeInt(TIMESTAMP_EOF);
                mouse.writeInt(TIMESTAMP_EOF);
                keyboard.close();
                mouse.close();
            }

            output = null;
            input = null;
            keys = null;
            keyboard = null;
            mouse = null;

            retained_timestamp = TIMESTAMP_EOF;
            retained_bytes = null;

            System.out.println("client.client.Replay recording stopped");
        } catch (Exception e) {
            output = null;
            input = null;
            keys = null;
            keyboard = null;
            mouse = null;
            System.err.println("Unable to close replay files");
            return;
        }

    }

    public static void update() {
        // Increment the replay timestamp
        if (!Replay.isPlaying)
            Replay.incrementTimestamp();
    }

    public static void shutdown_error() {
        closeReplayRecording();
        System.err.println("Recording has been stopped because of an error");
        System.err.println("Please log back in to start recording again");
    }

    public static void dumpKeyboardInput(int keycode, byte event, char keychar, int modifier) {
        if (keyboard == null)
            return;

        try {
            keyboard.writeInt(timestamp);
            keyboard.writeByte(event);
            keyboard.writeChar(keychar);
            keyboard.writeInt(keycode);
            keyboard.writeInt(modifier);
        } catch (Exception e) {
            e.printStackTrace();
            shutdown_error();
        }
    }

    public static void dumpMouseInput(byte event, int x, int y, int rotation, int modifier, int clickCount, int scrollType, int scrollAmount, boolean popupTrigger, int button) {
        if (mouse == null)
            return;

        try {
            mouse.writeInt(timestamp);
            mouse.writeByte(event);
            mouse.writeInt(x);
            mouse.writeInt(y);
            mouse.writeInt(rotation);
            mouse.writeInt(modifier);
            mouse.writeInt(clickCount);
            mouse.writeInt(scrollType);
            mouse.writeInt(scrollAmount);
            mouse.writeBoolean(popupTrigger);
            mouse.writeInt(button);
        } catch (Exception e) {
            e.printStackTrace();
            shutdown_error();
        }
    }

    public static void dumpRawInputStream(byte[] b, int n, int n2, int n5, int bytesread) {
        // Save timestamp of last time we saw data from the server
        if (bytesread > 0) {
            int lag = timestamp - timestamp_server_last;
            if (lag > 10)
                timestamp_lag = lag;
            timestamp_server_last = timestamp;
        }

        if (input == null)
            return;

        int off = n2 + n5;
        // when packet 182 is received retained_timestamp should be TIMESTAMP_EOF
        // to indicate not to dump previous packet
        if (retained_timestamp != TIMESTAMP_EOF) {
            // new set of packets arrived, dump previous ones
            try {
                input.writeInt(retained_timestamp);
                input.writeInt(retained_bread);
                input.write(retained_bytes, retained_off, retained_bread);
            } catch (Exception e) {
                e.printStackTrace();
                shutdown_error();
            }
        }
        retained_timestamp = timestamp;
        // Important! Cloned since it gets modified by decryption in game logic
        retained_bytes = b.clone();
        retained_off = off;
        retained_bread = bytesread;
    }

    public static void dumpRawOutputStream(byte[] b, int off, int len) {
        if (output == null)
            return;

        try {
            boolean isLogin = false;
            int pos = -1;
            byte[] out_b = null;
            // for the first bytes if byte == (byte)Client.version, 4 bytes before indicate if its
            // login or reconnect and 5 its what determines if its login-related
            for (int i = off + 5; i < off + Math.min(15, len); i++) {
                if (b[i] == (byte)235 && b[i - 5] == 0) {
                    isLogin = true;
                    pos = i + 1;
                    out_b = b.clone();
                    break;
                }
            }
            if (isLogin && pos != -1) {
                for (int i = pos; i < off + len; i++) {
                    out_b[i] = 0x00;
                }

                System.out.println("client.client.Replay: Removed login block from client output");

                output.writeInt(timestamp);
                output.writeInt(len);
                output.write(out_b, off, len);
                return;
            }

            output.writeInt(timestamp);
            output.writeInt(len);
            output.write(b, off, len);
        } catch (Exception e) {
            e.printStackTrace();
            shutdown_error();
        }
    }

    public static int hookXTEAKey(int key) {
        if (play_keys != null) {
            try {
                return play_keys.readInt();
            } catch (Exception e) {
                // e.printStackTrace();
                shutdown_error();
                return key;
            }
        }

        if (keys == null)
            return key;

        try {
            keys.writeInt(key); // data length
        } catch (Exception e) {
            // e.printStackTrace();
            shutdown_error();
        }

        return key;
    }

    public static void saveEncOpcode(int inopcode) {
        if (isRecording) {
            enc_opcode = inopcode;
        }
    }

    public static void checkPoint(int opcode, int len) {
        if (input == null)
            return;

        if (isRecording) {
            // received packet 182, set flag, do not dump bytes
            if (opcode == 182) {
                // in here probably would need to check the position
                // don't care about the packet if 182, just rewrite it using the enc opcode
                try {
                    input.writeInt(retained_timestamp);
                    input.writeInt(retained_bread);
                    retained_bytes[retained_off + 1] = (byte)127;
                    retained_bytes[retained_off + 2] = 0;
                    retained_bytes[retained_off + 3] = 0;
                    retained_bytes[retained_off + 4] = 1;
                    input.write(retained_bytes, retained_off, retained_bread);
                    System.out.println("client.client.Replay: Removed host block from client input");
                } catch (Exception e) {
                    e.printStackTrace();
                    shutdown_error();
                }
                retained_timestamp = TIMESTAMP_EOF;
                // free memory
                retained_bytes = null;
            }
        }
    }

}
